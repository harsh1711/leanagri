package com.leancrop.application.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.leancrop.application.R;
import com.leancrop.application.utils.ApplicationPreferences;

public class FilterDialogFragment extends DialogFragment {

    private RadioGroup filterRadioGroup;
    private RadioButton alpha,rating,date;
    private OnFilterSelectedListener onFilterSelected;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    @NonNull
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filter_dialog_fragment, container, false);

        iniIds(view);
        setLastSelectedFilter();
        handleClickEvent();

        return view;
    }

    private void handleClickEvent() {
        filterRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.alpha) {
                    callFilterCallback(alpha.getText().toString());
                } else  if (checkedId == R.id.rating) {
                    callFilterCallback(rating.getText().toString());
                } else  if (checkedId == R.id.date) {
                    callFilterCallback(date.getText().toString());
                }

            }
        });
    }

    private void setLastSelectedFilter() {
        String filterByStr = ApplicationPreferences.get().getFilterBy();
        if(filterByStr != null){
            if (filterByStr.equalsIgnoreCase(getString(R.string.alphabetically))) {
                alpha.setChecked(true);
            } else if (filterByStr.equalsIgnoreCase(getString(R.string.rating))) {
                rating.setChecked(true);
            } else if (filterByStr.equalsIgnoreCase(getString(R.string.release_date))) {
                date.setChecked(true);
            }
        }

    }

    private void iniIds(View view) {
        filterRadioGroup = view.findViewById(R.id.filterRadioGroup);
        alpha = view.findViewById(R.id.alpha);
        rating = view.findViewById(R.id.rating);
        date = view.findViewById(R.id.date);
    }

    private void callFilterCallback(String filterBy) {
        if(onFilterSelected != null){
            ApplicationPreferences.get().setFilterBy(filterBy);
            onFilterSelected.onFilterSelected();
            dismiss();
        }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFilterSelectedListener) {
            //init the listener
            onFilterSelected = (OnFilterSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FilterSelectedListener");
        }
    }

    public interface OnFilterSelectedListener {
         void onFilterSelected();
    }
}
