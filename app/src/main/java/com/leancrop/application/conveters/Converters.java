package com.leancrop.application.conveters;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.leancrop.application.model.Results;

import java.util.List;

public class Converters {

    @TypeConverter
    public static List<Results> restoreList(String listOfString) {
        return new Gson().fromJson(listOfString, new TypeToken<List<Results>>() {}.getType());
    }

    @TypeConverter
    public static String saveList(List<Results> listOfString) {
        return new Gson().toJson(listOfString);
    }
}
