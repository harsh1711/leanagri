package com.leancrop.application.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.leancrop.application.R;
import com.leancrop.application.activity.DetailsActivity;
import com.leancrop.application.model.Results;

import java.util.List;
import java.util.Locale;

public class MovieAdapter extends ArrayAdapter<Results> {

    private List<Results> list;
    private Context context;
    private LayoutInflater inflater;

    public MovieAdapter(Context context, int resource, List<Results> list) {
        super(context, resource, list);
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        MovieAdapter.ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.movie_list, null);
            holder = new MovieAdapter.ViewHolder();

            holder.movieName = convertView.findViewById(R.id.movieName);
            holder.parenLayout = convertView.findViewById(R.id.parenLayout);

            convertView.setTag(holder);
        }
        else {
            holder = (MovieAdapter.ViewHolder) convertView.getTag();
        }

        final Results item = getItem(position);
        if(item != null){
            String title = item.getTitle();
            if(title.length() > 0){
                holder.movieName.setText(title);
            }


            holder.parenLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context,DetailsActivity.class);
                    intent.putExtra(DetailsActivity.MOVIE_DETAILS,new Gson().toJson(item));
                    context.startActivity(intent);
                }
            });
        }

        return convertView;
    }


    @Override
    public Results getItem(int position) {
        return list.get(position);
    }



    @Override
    public int getCount() {
        if(list == null || list.size() == 0){
            return 0;
        }
        return list.size();
    }

    public List<Results> getList() {
        return list;
    }

    private class ViewHolder {
        private TextView movieName;
        private RelativeLayout parenLayout;
    }
}
