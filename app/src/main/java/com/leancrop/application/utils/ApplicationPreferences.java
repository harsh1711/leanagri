package com.leancrop.application.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.leancrop.application.R;



public class ApplicationPreferences {

    // KEYS used in this app ///////////////////////////////////
    private static final String KEY_USER_LOGGED_IN = "loggedIn";
    private static final String FILTER_BY = "FILTER_BY";

    ////////////////////////////////////////////////////////////

    private static String spFileKey = ApplicationPreferences.class.getName() + ".prefs";
    private static ApplicationPreferences instance;

    private final SharedPreferences sharedPrefs;

    private ApplicationPreferences(Context context) {
        this.sharedPrefs = context.getSharedPreferences(spFileKey, Context.MODE_PRIVATE);
    }

    private void setBooleanValue(String key, Boolean value) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private void setStringValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void init(Context context) {
        instance = new ApplicationPreferences(context);
    }

    public static ApplicationPreferences get() {
        if (instance == null) {
            throw new IllegalStateException("ApplicationPreferences should be initialized by calling init()");
        }
        return instance;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    ///// Add Application specific prefs below ///////////
    /////////////////////////////////////////////////////////////////////////////////////////////

    public void setUserLoggedIn(boolean loggedIn) {
        setBooleanValue(KEY_USER_LOGGED_IN, loggedIn);
    }

    public String setFilterBy(String filterName) {
        setStringValue(FILTER_BY, filterName);
        return filterName;
    }

    public String getFilterBy() {
        return sharedPrefs.getString(FILTER_BY, null);
    }
}
