package com.leancrop.application.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.leancrop.application.conveters.Converters;
import com.leancrop.application.dao.MovieDao;
import com.leancrop.application.model.Movie;
import com.leancrop.application.model.Results;


@Database(entities = {Movie.class,Results.class},version = 1)
@TypeConverters(Converters.class)
public abstract class RoomDBHelper extends RoomDatabase {

    private static RoomDBHelper ourInstance = null;

    public static RoomDBHelper init(Context context, String appName) {
        if(ourInstance == null) {
            ourInstance = Room.databaseBuilder(context,
                    RoomDBHelper.class, appName).build();
        }
        return ourInstance;
    }

    public static RoomDBHelper get() {
        if(ourInstance == null) {
            throw new IllegalStateException("Call init to initialize the room database before using it.");
        }
        return ourInstance;
    }

    public abstract MovieDao movieDao();
}
