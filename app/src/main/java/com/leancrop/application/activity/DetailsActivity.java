package com.leancrop.application.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;
import com.leancrop.application.R;
import com.leancrop.application.model.Results;

public class DetailsActivity extends AppCompatActivity {

    public static final String MOVIE_DETAILS = "MOVIE_DETAILS";
    private Results results;
    private TextView languageTv,movieTitle,ratingTv,releaseDateTv,overviewTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        setUpToolbar();
        iniIds();
        getIntentValue(getIntent());
    }

    private void iniIds() {
        movieTitle = findViewById(R.id.movieTitle);
        languageTv = findViewById(R.id.languageTv);
        ratingTv = findViewById(R.id.ratingTv);
        releaseDateTv = findViewById(R.id.releaseDateTv);
        overviewTv = findViewById(R.id.overviewTv);
    }

    private void getIntentValue(Intent intent) {
        if(intent != null){
            String movieDetailsStr = intent.getStringExtra(MOVIE_DETAILS);
            if(movieDetailsStr != null){
                Gson gson = new Gson();
                results = gson.fromJson(movieDetailsStr,Results.class);
                updateDetails();
            }
        }
    }

    private void updateDetails() {
        if(results != null){
            movieTitle.setText(results.getTitle());
            languageTv.setText(results.getOriginal_language());
            String rating = results.getVote_average() + getString(R.string.divide_by_ten);
            ratingTv.setText(rating);
            releaseDateTv.setText(results.getRelease_date());
            overviewTv.setText(results.getOverview());
        }
    }

    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Drawable backArrow = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black);
        getSupportActionBar().setHomeAsUpIndicator(backArrow);
        getSupportActionBar().setTitle(getString(R.string.movie_details));
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.textColor));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
