package com.leancrop.application.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.leancrop.application.R;
import com.leancrop.application.adapter.MovieAdapter;
import com.leancrop.application.api.APIUrls;
import com.leancrop.application.db.RoomDBHelper;
import com.leancrop.application.fragments.FilterDialogFragment;
import com.leancrop.application.model.Movie;
import com.leancrop.application.model.Results;
import com.leancrop.application.net.ApiService;
import com.leancrop.application.net.GenericRequest;
import com.leancrop.application.utils.AppUtils;
import com.leancrop.application.utils.ApplicationPreferences;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements FilterDialogFragment.OnFilterSelectedListener {

    private ListView movieListView;
    private MovieAdapter movieAdapter;
    private RelativeLayout parentLl;
    private Context context;
    private RoomDBHelper roomDBHelper;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniIds();
        // getting movies from db
        new GetMovieFromDbTask(this).execute();
    }

    private void callMovieAPI() {
        showProgressBar();
        GenericRequest<Movie> movieRequest = new GenericRequest<Movie>
                (Request.Method.GET, APIUrls.get().GET_MOVIES(),
                        Movie.class, null,
                        new Response.Listener<Movie>() {
                            @Override
                            public void onResponse(Movie movie) {
                                new InsertMovieIntoDbTask(MainActivity.this).execute(movie);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                hideProgressBar();
                                String errorMsg = AppUtils.getVolleyError(context, error);
                                AppUtils.openSnackBar(parentLl, errorMsg);
                            }
                        });

        ApiService.get().addToRequestQueue(movieRequest);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void updateMovieList(Movie movie) {
        if (movie != null) {
            List<Results> resultsList = movie.getResults();
            ApplicationPreferences.get().setFilterBy(getString(R.string.alphabetically));
            sortListAlphabetically(resultsList);
            movieAdapter = new MovieAdapter(context, R.layout.movie_list, resultsList);
            movieListView.setAdapter(movieAdapter);
        }

    }

    private void iniIds() {
        context = this;
        movieListView = findViewById(R.id.movieList);
        parentLl = findViewById(R.id.activity_main);
        progressBar = findViewById(R.id.progressBar);
        roomDBHelper = RoomDBHelper.get();
    }

    private static class InsertMovieIntoDbTask extends AsyncTask<Movie, Void, Movie> {

        private WeakReference<MainActivity> activityReference;

        InsertMovieIntoDbTask(MainActivity context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Movie doInBackground(Movie... movies) {
            if (activityReference.get() != null) {
                activityReference.get().roomDBHelper.movieDao().insertAll(movies);
                return activityReference.get().roomDBHelper.movieDao().fetchAllMovies();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Movie movie) {
            if (activityReference.get() != null) {
                activityReference.get().hideProgressBar();
                if (movie != null) {
                    activityReference.get().updateMovieList(movie);
                }
            }
        }
    }

    private static class GetMovieFromDbTask extends AsyncTask<Void, Void, Movie> {

        private WeakReference<MainActivity> activityReference;

        GetMovieFromDbTask(MainActivity context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected Movie doInBackground(Void... voids) {
            return activityReference.get().roomDBHelper.movieDao().fetchAllMovies();
        }

        @Override
        protected void onPostExecute(Movie movie) {
            if (activityReference.get() != null) {
                if (movie != null) {
                    // movies are available updating list
                    activityReference.get().updateMovieList(movie);
                }else {
                    // movies are not available checking net and fetching movies
                    activityReference.get().checkIfInternetIsAvailable();
                }
            }
        }
    }

    private void checkIfInternetIsAvailable() {
        if(context != null){
            if(AppUtils.isNetworkAvailable(context)){
                // network is available calling api to get movies
                callMovieAPI();
            }else {
                // showing no conn dialog box
                openDialogBoxForNoInternetConnection();
            }
        }
    }

    private void openDialogBoxForNoInternetConnection() {
        if(context != null){
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View promptView = layoutInflater.inflate(R.layout.dialog_box, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
            alertDialogBuilder.setView(promptView);

            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            checkIfInternetIsAvailable();
                        }
                    });

            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter, menu);
        // return true so that the menu pop up is opened
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.filter) {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            android.support.v4.app.Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            DialogFragment dialogFragment = new FilterDialogFragment();
            dialogFragment.show(ft, "dialog");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFilterSelected() {
        String filterByStr = ApplicationPreferences.get().getFilterBy();
        if (filterByStr != null && movieAdapter != null) {
            ApplicationPreferences.get().setFilterBy(filterByStr);
            List<Results> resultsList = movieAdapter.getList();
            if (filterByStr.equalsIgnoreCase(getString(R.string.alphabetically))) {
                sortListAlphabetically(resultsList);
            } else if (filterByStr.equalsIgnoreCase(getString(R.string.rating))) {
                sortListByRating(resultsList);
            } else if (filterByStr.equalsIgnoreCase(getString(R.string.release_date))) {
                sortListByReleaseDate(resultsList);
            }
            movieAdapter.notifyDataSetChanged();
        }
    }

    private void sortListAlphabetically(List<Results> resultsList) {
        Collections.sort(resultsList, new Comparator<Results>() {
            @Override
            public int compare(Results results1, Results results2) {
                return (results1.getTitle().compareTo(results2.getTitle()));
            }

        });
    }

    private void sortListByReleaseDate(List<Results> resultsList) {
        Collections.sort(resultsList, new Comparator<Results>() {
            @Override
            public int compare(Results results1, Results results2) {

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date1 = format.parse(results2.getRelease_date());
                    Date date2 = format.parse(results1.getRelease_date());
                    return date1.compareTo(date2);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
    }

    private void sortListByRating(List<Results> resultsList) {
        Collections.sort(resultsList, new Comparator<Results>() {
            @Override
            public int compare(Results results1, Results results2) {
                return Double.compare(results2.getVote_average(), results1.getVote_average());
            }

        });
    }
}
