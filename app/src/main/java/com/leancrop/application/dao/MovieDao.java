package com.leancrop.application.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.leancrop.application.conveters.Converters;
import com.leancrop.application.model.Movie;

@Dao
public interface MovieDao {

    @Insert()
    @TypeConverters(Converters.class)
    void insertAll(Movie... results);


    @Query("SELECT * FROM Movie")
    Movie fetchAllMovies();
}
