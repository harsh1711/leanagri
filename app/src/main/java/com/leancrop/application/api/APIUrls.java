package com.leancrop.application.api;


public class APIUrls {
    private static APIUrls instance;
    private String apiPrefix;

    public APIUrls(String prefix) {
        this.apiPrefix = prefix;
    }

    public static APIUrls init(String prefix) {
        instance = new APIUrls(prefix);
        return instance;
    }

    public static APIUrls get() {
        return instance;
    }

    public String GET_MOVIES() {
        return "https://api.themoviedb.org/3/trending/movie/week?api_key=05b7a9caa5ce977d13a5c267b4e490c1";
    }
}
