package com.leancrop.application;

import android.app.Application;

import com.leancrop.application.api.APIUrls;
import com.leancrop.application.db.RoomDBHelper;
import com.leancrop.application.net.ApiService;
import com.leancrop.application.utils.ApplicationPreferences;
import com.leancrop.application.utils.TypefaceUtil;


public class LeanCropApplication extends Application {
    private static final String APP_NAME = "LeanCropApplication";

    public static final int PRODUCTION = 0;
    public static final int TEST = 1;

    // Change this mode to utilize test or production URLS
    public static final int APP_MODE = TEST;

    private static final String TEST_URL_PREFIX = "http://test.example.com:8080/v1";
    private static final String PROD_URL_PREFIX = "https://api.example.com/v1";

    @Override
    public void onCreate() {
        // Override default font if required. Uncomment the following to revert to
        // default android behaviour
        TypefaceUtil.overrideFonts(getApplicationContext());

        // Initialize Application Preferences. Use ApplicationPreferences singleton
        // to access preferences from any actitivy or fragment.
        ApplicationPreferences.init(getApplicationContext());

        // Initialize Database object. Use a singleton object to provide access to
        // the database helper
        RoomDBHelper.init(getApplicationContext(),APP_NAME);

        // Initialize Volley for network calls
        ApiService.init(getApplicationContext());

        // Initialize API URLs to point to correct server location (prod/test)
        APIUrls.init(getURLPrefix());

        super.onCreate();
    }

    private String getURLPrefix() {
        if(APP_MODE == TEST) {
            return TEST_URL_PREFIX;
        } else {
            return PROD_URL_PREFIX;
        }
    }
}
